
#include "InverseMultimap.h"

namespace kalinskymalinsky_InverseMultimap {
    InverseMultimap::InverseMultimap(CounterMap const &inputMap) {
        auto it = inputMap.getStartIterator();
        for (; it != inputMap.getEndIterator(); it++) {
            Multimap.insert(make_pair(it->second, it->first));
        }
    }
};