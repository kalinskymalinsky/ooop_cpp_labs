
#include "CounterMap.h"

using std::istream;
using std::string;
using namespace kalinskymalinsky_CounterMapConstants;

namespace kalinskymalinsky_CounterMap {
    CounterMap::CounterMap() {
        numberOfWords_ = 0;
    }

    void CounterMap::parseAndCount(istream &fin) {
        string buffer, word;
        while (getline(fin, buffer)) {
            for (int i = 0; i <= buffer.size(); i++) {
                if (isNumberOrLetter(buffer[i])) { // Собираем слово до первого разделителя
                    word += buffer[i];
                } else if (word != EMPTY_WORD) {  // Отсекаем случаи, когда разделители идут подряд
                    this->increaseWordMapCounter(word);
                    word = EMPTY_WORD;
                }
            }
        }
    }

    int CounterMap::isNumberOrLetter(char symbol) {
        if (((LOWER_NUMBER_BOUND <= symbol) && (symbol <= UPPER_NUMBER_BOUND))
            || ((LOWER_SYMBOL_BOUND <= symbol) && (symbol <= UPPER_SYMBOL_BOUND))
                )
            return 1;
        else return 0;
    }

    map<string, int>::const_iterator CounterMap::getStartIterator() const {
        return this->Map.begin();
    }

    map<string, int>::const_iterator CounterMap::getEndIterator() const {
        return this->Map.end();
    }

    int CounterMap::getNumberOfWords() const {
        return this->numberOfWords_;
    }

    void CounterMap::increaseWordMapCounter(string word) {
        this->Map[word]++;
        this->numberOfWords_++;
    }
};