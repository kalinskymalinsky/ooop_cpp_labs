#include <iostream>
#include <fstream>
#include "Writer.h"

using std::ifstream;
using std::cout;
using std::endl;
using kalinskymalinsky_Writer::Writer;
using kalinskymalinsky_CounterMapConstants::INPUT_PARAMETER_NUMBER;
using kalinskymalinsky_CounterMapConstants::OUTPUT_PARAMETER_NUMBER;
using kalinskymalinsky_CounterMapConstants::INPUT_FILE_NOT_FOUND_MESSAGE;

int main( int argc, char * argv[] ) {

    CounterMap counterTable;
    ifstream fin( argv[INPUT_PARAMETER_NUMBER] );
    if (!fin) {
        cout << INPUT_FILE_NOT_FOUND_MESSAGE << endl;
        return 0;
    }

    Writer output( argv[OUTPUT_PARAMETER_NUMBER] );

    counterTable.parseAndCount( fin );
    InverseMultimap inverseCounterTable( counterTable );
    output.writeMapInCSV( inverseCounterTable, counterTable.getNumberOfWords() );

    fin.close();
    return 0;
}
