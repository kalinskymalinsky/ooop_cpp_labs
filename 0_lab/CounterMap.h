

#ifndef INC_0_LAB_COUNTERMAP_H
#define INC_0_LAB_COUNTERMAP_H
#include <map>
#include <fstream>

namespace kalinskymalinsky_CounterMapConstants {
    const char LOWER_SYMBOL_BOUND = 'A';
    const char UPPER_SYMBOL_BOUND = 'z';
    const char LOWER_NUMBER_BOUND = '0';
    const char UPPER_NUMBER_BOUND = '9';
    const int INPUT_PARAMETER_NUMBER = 1;
    const int OUTPUT_PARAMETER_NUMBER = 2;
    const std::string EMPTY_WORD = "";
    const std::string WORDS_NOT_FOUND_MESSAGE = "Words are not found";
    const std::string INPUT_FILE_NOT_FOUND_MESSAGE = "Input file not found";
    const int FREQUENCY_TO_PERCENT_MULTIPLIER = 100;
    const char COLUMN_DELIMITER = ',';
    const char PERCENT_SYMBOL = '%';
    const int NO_WORDS = 0;
};

using std::string;
using std::map;

namespace kalinskymalinsky_CounterMap {
    class CounterMap {
    public:

        CounterMap();

        void parseAndCount(std::istream &fin);

        static int isNumberOrLetter(char symbol);

        int getNumberOfWords() const;

        map<string, int>::const_iterator getStartIterator() const;

        map<string, int>::const_iterator getEndIterator() const;

    private:
        int numberOfWords_;
        std::map<std::string, int> Map;

        void increaseWordMapCounter(std::string word);
    };
};


#endif //INC_0_LAB_COUNTERMAP_H
