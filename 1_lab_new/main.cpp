/*#include <iostream>
#include <math.h>

// False = 00, Unknown = 10, True = 11
enum TritValues { False, Unknown = 2, True};

class TritSet {
public:
    TritSet( uint32_t numberOfTrits ) {
        tritArraySize_ = numberOfTrits;
        lastInitializedTrit_ = 0;
        tritArray = new uint32_t [ (int)ceil( (double)numberOfTrits * NUM_OF_TRITS_IN_UINT ) ];
    }

    class Trit {
     public:
        Trit( uint32_t & arrayValue, uint32_t position, uint32_t subPosition, TritValues tritValue) : arrayValue_( arrayValue ), position_(position), subPosition_(subPosition), tritValue_(tritValue) {}

        Trit &operator= ( TritValues const & value) {
            arrayValue_ &= ~( TRIT_MASK << getShift( subPosition_ ) );
            arrayValue_ |= ( value << getShift( subPosition_ ) );
            tritValue_ = value;
            return *this;
        }
        Trit &operator= ( Trit const & other) {
            arrayValue_ &= ~( TRIT_MASK << getShift( subPosition_ ) );
            arrayValue_ |= ( other.tritValue_ << getShift( subPosition_ ) );
            tritValue_ = other.tritValue_;
            return *this;
        }
        operator TritValues(){
            return this->tritValue_;
        }
     private:
        uint32_t & arrayValue_;
        uint32_t position_;
        uint32_t subPosition_;
        TritValues tritValue_;
    };

    static uint32_t getPosition( uint32_t const & index ) {
        return index / NUM_OF_TRITS_IN_UINT;
    }
    static uint32_t getSubPosition( uint32_t const & index ) {
        uint32_t temp;
        return index % NUM_OF_TRITS_IN_UINT;
    }
    TritValues getTritValue( uint32_t const & index ) const {
        uint32_t value = 0;
        uint32_t mask = TRIT_MASK;
        mask <<= getShift( index );
        value = tritArray[getPosition( index )] & mask;
        value >>= getShift( index );
        return (TritValues)value;
    }
    static int getShift( uint32_t const & index ) {
        return ( NUM_OF_TRITS_IN_UINT - getSubPosition( index ) - 1 ) * 2;
    }

    Trit operator []( uint32_t const & index ) {
        if ( index < tritArraySize_ ) {
            return Trit( tritArray[getPosition( index )], getPosition( index ), getSubPosition( index ), getTritValue( index ) );
        }
        else {
            return Trit( tritArray[0], getPosition( index ), getSubPosition( index ), Unknown );
        }
    }

    size_t capacity() {
        return this->tritArraySize_;
    }

 private:
    uint32_t tritArraySize_;
    uint32_t lastInitializedTrit_;
    uint32_t * tritArray;
};
*/
#include "TritSet.h"
#include "TritValues.h"
#include <gtest/gtest.h>



int main() {
    using kalinskymalinsky_TritSet::TritSet;
    using namespace kalinskymalinsky_TritValues;
    TritSet set(10);
    TritSet set1(100);
    TritSet set2(10);

    for (int i = 0; i < 10; i++) {
        set[i] = True;
    }
    std::cout << " Logical size: " << set.length() << " set[33] == " << set[33] << std::endl;
    set[33] = True;
    for (int i = 0; i < 50; i++) {
        std::cout <<  i << " : " << set[i] << std::endl;
    }
    set = ~set;
    for (int i = 0; i < 50; i++) {
        std::cout <<  i << " : " << set[i] << std::endl;
    }
    /*
    std::cout << " Size: " << set.capacity() << " set[100] == " << set[100] << std::endl;
    for (int i = 0; i < 10; i++) {
        std::cout <<  set[i] << std::endl;
    }
    for (int i = 0; i < 10; i++) {
        set[i] = True;
    }
    for (int i = 0; i < 10; i++) {
        std::cout <<  set[i] << std::endl;
    }
    set[9] = True;
    std::cout << " Size: " << set.capacity() << " set[9] == " << set[9] << std::endl;
    set[100] = set[9];
    set[100] = set[100];
    for (int i = 0; i < 10; i++) {
        std::cout <<  set[i] << std::endl;
    }
    std::cout << " Size after: " << set.capacity() << " set[100] == " << set[100] << std::endl;
    std::cout << " Size: " << set.capacity() << " set[100] == " << set[100] << std::endl;
    set[100] = Unknown;
    std::cout << " Size: " << set.capacity() << " set[100] == " << set[100] << std::endl;
    set.shrink();
    std::cout << " Size: " << set.capacity() << " set[100] == " << set[100] << std::endl;
    for (int i = 0; i <= 100; i++) {
        std::cout << i << ' ' << set[i] << std::endl;
    }*/



    return 0;
}
